/**
 * environment de Produção
 */
export const environment = {
  production: true,
  apiUrl: 'https://menudigital-api.herokuapp.com',
};
