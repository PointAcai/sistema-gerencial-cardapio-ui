import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Oferta } from '../model/oferta';
import { environment } from './../../environments/environment';
import { SisHttp } from './../seguranca/sis-http';

@Injectable()
export class OfertaService {

  ofertaUrl: string;
  produtoUrl: string;

  constructor(private http: SisHttp) {
    this.ofertaUrl = `${environment.apiUrl}/ofertas`;
    this.produtoUrl = `${environment.apiUrl}/produtos`;
  }

  // Método de Pesquisar com a Paginação
  pesquisar(): Promise<any> {
    return this.http.get<any>(`${this.ofertaUrl}?resumo`).toPromise();
  }

  listarOfertas(): Promise<any> {
    return this.http.get(`${this.ofertaUrl}`).toPromise();
  }

  // Método de Excluir
  excluir(codigo: number): Promise<void> {
    return this.http.delete<any>(`${this.ofertaUrl}/${codigo}`).toPromise();
  }

  // Método de Adicionar
  adicionar(oferta: Oferta): Promise<Oferta> {
    return this.http.post<Oferta>(this.ofertaUrl, oferta).toPromise();
  }

  // Método de Atualizar
  atualizar(oferta: Oferta): Promise<Oferta> {
    return this.http.put<Oferta>(`${this.ofertaUrl}/${oferta.codigo}`, oferta).toPromise();
  }

  // Método para Alterar a Propriedade Status
  alterarStatus(codigo: number, ativo: boolean): Promise<void> {
    const headers = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.put(`${this.ofertaUrl}/${codigo}/status`, ativo, { headers} ).toPromise().then(() => null);
  }

  buscarPorCodigo(codigo: number): Promise<Oferta> {
    return this.http.get<Oferta>(`${this.ofertaUrl}/${codigo}`).toPromise();
  }

  listarProdutos(oferta: string): Promise<any> {
    const params = new HttpParams().append('isOferta', oferta.toString());
    return this.http.get(`${this.produtoUrl}?ofertas`, { params }).toPromise();
  }

}
