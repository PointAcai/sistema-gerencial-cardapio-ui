import { AuthGuard } from './../seguranca/auth.guard';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { OfertaCadastroComponent } from './ofertas-cadastro/oferta-cadastro.component';
import { OfertaPesquisaComponent } from './ofertas-pesquisa/oferta-pesquisa.component';

const routes: Routes = [
  // canActivate é utilizado para Proteger os Links de Navegação
  {
    // Página de Pesquisa
    path: '',
    component: OfertaPesquisaComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_PESQUISAR_OFERTA'] }
  },
  {
    // Página de Cadastro
    path: 'cadastrar',
    component: OfertaCadastroComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_CADASTRAR_OFERTA'] }
  },
  {
    // Página de Alteração
    path: ':codigo',
    component: OfertaCadastroComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_ALTERAR_OFERTA'] }
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class OfertaRoutingModule { }
