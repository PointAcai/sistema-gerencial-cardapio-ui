import { Component, OnInit } from '@angular/core';
import { Oferta } from 'app/model/oferta';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationService } from '../../../../node_modules/primeng/components/common/api';
import { ErrorHandlerService } from './../../core/error-handler.service';
import { AuthService } from './../../seguranca/auth.service';
import { OfertaService } from './../oferta.service';

@Component({
  selector: 'app-oferta-pesquisa',
  templateUrl: './oferta-pesquisa.component.html',
  styleUrls: ['./oferta-pesquisa.component.css']
})
export class OfertaPesquisaComponent implements OnInit {

  ofertas: Oferta[];
  cols: any[];

  constructor(
    private auth: AuthService,
    private ofertaService: OfertaService,
    private toastr: ToastrService,
    private confirmation: ConfirmationService,
    private errorHandler: ErrorHandlerService,
  ) { }

  ngOnInit() {
    this.pesquisar();
  }

  pesquisar() {
    this.ofertaService.pesquisar().then(resultado => {
      this.ofertas = resultado;
    }).catch(erro => this.errorHandler.handle(erro));

    this.cols = [
      { field: 'codigo', header: 'Código' },
      { field: 'descricao', header: 'Descrição' },
      { field: 'valorPromocional', header: 'Valor' },
      { field: 'dataValidadeInicio', header: 'Data Início' },
      { field: 'dataValidadeFim', header: 'Data Fim' },
      { field: 'statusOferta', header: 'Status' },
      { field: 'codigo', header: '' }
    ];
  }

  confirmarExcluir(oferta: any) {
    this.confirmation.confirm({
      message: 'Deseja Realmente Excluir esta Oferta?',
      accept: () => {
        this.excluir(oferta);
      }
    });
  }

  excluir(oferta: any) {
    this.ofertaService.excluir(oferta.codigo).then(() => {
      this.pesquisar();
      this.toastr.success('Oferta Excluída com Sucesso!');
    }).catch(erro => this.errorHandler.handle(erro));
  }

  alterarStatus(oferta: any): void {
    const novoStatus = !oferta.statusOferta;

    this.ofertaService.alterarStatus(oferta.codigo, novoStatus)
      .then(() => {
        const acao = novoStatus ? 'Ativada' : 'Desativada';
        oferta.statusOferta = novoStatus;
        this.toastr.success(`Oferta ${acao} com Sucesso!`);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

}
