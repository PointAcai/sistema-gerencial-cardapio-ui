import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';

import { FormGroup, FormBuilder, Validators, FormControl } from '../../../../node_modules/@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '../../../../node_modules/@angular/router';

import { ErrorHandlerService } from './../../core/error-handler.service';
import { OfertaService } from './../oferta.service';
import { Oferta } from '../../model/oferta';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-oferta-cadastro',
  templateUrl: './oferta-cadastro.component.html',
  styleUrls: ['./oferta-cadastro.component.css'],
})
export class OfertaCadastroComponent implements OnInit {

  formulario: FormGroup;

  produtos: SelectItem[];

  ptBr: any;

  constructor(
    private ofertaService: OfertaService,
    private errorHandler: ErrorHandlerService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    // Configuração do Formulário Reativo
    this.configurarFormularioReativo();

    const codigoOferta = this.route.snapshot.params['codigo'];
    if (codigoOferta) {
      this.carregarOferta(codigoOferta);
    } else {
      this.listarProdutos();
    }

    this.ptBr = {
      firstDayOfWeek: 0,
      dayNames: ['Domingo', 'Segunda', 'terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
      dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
      dayNamesMin: ['Do', 'Se', 'Te', 'Qa', 'Qi', 'Sx', 'Sb'],
      monthNames: [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto',
      'Setembro', 'Outubro', 'Novembro', 'Dezembro' ],
      monthNamesShort: [ 'Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez' ],
      today: 'Hoje',
      clear: 'Limpar',
      dateFormat: 'mm/dd/yy'
    };

  }

  listarProdutos() {
    this.ofertaService.listarProdutos('NÃO')
      .then(produto => {
        const produtosBanco = produto.content;
        this.produtos = produtosBanco.map(p =>
          ({ label: p.descricao, value: p.codigo })
        );
        this.produtos.splice(0, 0, {label: 'Selecione um Produto', value: null});
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  configurarFormularioReativo() {
    this.formulario = this.formBuilder.group({
      statusOferta: true,
      dataValidade: '',
      descricao: '',
      codigo: null,
      valorPromocional: '',
      produto: this.formBuilder.group({
        codigo: [],
        descricao: ''
      })
    });
  }

  validarObrigatoriedade(input: FormControl) {
    return (input.value ? null : { obrigatoriedade: true });
  }

  carregarOferta(codigo: number) {
    this.ofertaService.buscarPorCodigo(codigo)
      .then(oferta => {
        const dataValidade: Array<Date> = [];
        dataValidade.push(new Date(oferta.dataValidade[0]));
        dataValidade.push(new Date(oferta.dataValidade[1]));
        this.formulario.patchValue({
          codigo: oferta.codigo,
          dataValidade: dataValidade,
          valorPromocional: oferta.valorPromocional,
          descricao: oferta.descricao,
          produto: ({
            codigo: oferta.produto.codigo,
            descricao: oferta.produto.descricao
          })
        });
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  get editando() {
    return Boolean(this.formulario.get('codigo').value);
  }

  salvar() {
    if (this.editando) {
      this.atualizarOferta();
    } else {
      this.adicionarOferta();
    }
  }

  adicionarOferta() {
    this.ofertaService.adicionar(this.formulario.value)
      .then(ofertaAdicionado => {
        this.toastr.success('Oferta Adicionada com Sucesso!');
        this.router.navigate(['/ofertas']);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  atualizarOferta() {
    this.ofertaService.atualizar(this.formulario.value)
      .then(oferta => {
        this.formulario.patchValue(oferta);
        this.toastr.success('Oferta Atualizada com Sucesso!');
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  novo() {
    this.formulario.reset();
    setTimeout(function() {
      this.oferta = new Oferta();
    }.bind(this), 1);
    this.router.navigate(['/ofertas/cadastrar']);
  }

}
