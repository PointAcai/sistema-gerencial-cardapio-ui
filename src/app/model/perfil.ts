import { Permissao } from './permissao';

export class Perfil {
  codigo: number;
  descricao: string;
  permissoes: Array<Permissao> = [];
}
