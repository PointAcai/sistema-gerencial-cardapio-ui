export class Parametro {
  codigo: number;
  descricao: string;
  chave: string;
  valor: string;
  tipo: string;
  urlImagem: string;
}
