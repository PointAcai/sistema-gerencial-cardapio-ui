import { Produto } from './produto';

export class Oferta {
  codigo: number;
  dataValidade: Array<Date> = [];
  descricao: string;
  valorPromocional: number;
  produto = new Produto();
}
