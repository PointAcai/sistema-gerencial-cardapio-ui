import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';

import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';

import { ErrorHandlerService } from '../../core/error-handler.service';
import { ControlePedidoService } from '../controlepedido.service';
import { ControlePedido } from '../../../app/model/controlepedido';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-controlepedido-cadastro',
  templateUrl: './controlepedido-cadastro.component.html',
  styleUrls: ['./controlepedido-cadastro.component.css'],
})
export class ControlePedidosCadastroComponent implements OnInit {

  formulario: FormGroup;

  constructor(
    private controlePedidoService: ControlePedidoService,
    private errorHandler: ErrorHandlerService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.configurarFormularioReativo();

    const codigoControlePedido = this.route.snapshot.params['codigo'];
    if (codigoControlePedido) {
      this.carregarControlePedido(codigoControlePedido);
    }

  }

  configurarFormularioReativo() {
    this.formulario = this.formBuilder.group({
      codigo: null,
      senha: ['', [ this.validarObrigatoriedade ]],
      nome: ['']
    });
  }

  /**
   * VALIDAÇÕES
   */

  validarObrigatoriedade(input: FormControl) {
    return (input.value ? null : { obrigatoriedade: true });
  }

  /**
   * SERVIÇOS
   */

  carregarControlePedido(codigo: number) {
    this.controlePedidoService.buscarPorCodigo(codigo)
      .then(controlePedido => {
        this.formulario.patchValue(controlePedido);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  get editando() {
    return Boolean(this.formulario.get('codigo').value);
  }

  salvar() {
    if (this.editando) {
      this.atualizarControlePedido();
    } else {
      this.adicionarControlePedido();
    }
  }

  adicionarControlePedido() {
    this.controlePedidoService.adicionar(this.formulario.value)
      .then(controlePedidoAdicionado => {
        this.toastr.success('Senha Adicionada com Sucesso!');
        this.router.navigate(['/controlepedidos']);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  atualizarControlePedido() {
    this.controlePedidoService.atualizar(this.formulario.value)
      .then(controlePedido => {
        this.formulario.patchValue(controlePedido);
        this.toastr.success('Senha Atualizada com Sucesso!');
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  novo() {
    this.formulario.reset();
    setTimeout(function() {
      this.controlePedido = new ControlePedido;
    }.bind(this), 1);
    this.router.navigate(['/controlepedidos/cadastrar']);
  }

}
