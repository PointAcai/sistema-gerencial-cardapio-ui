import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ToolbarModule } from 'primeng/toolbar';
import { TableModule } from 'primeng/table';
import { TooltipModule } from 'primeng/tooltip';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { SelectButtonModule } from 'primeng/selectbutton';
import { InputMaskModule } from 'primeng/inputmask';
import { MultiSelectModule } from 'primeng/multiselect';
import { DropdownModule } from 'primeng/dropdown';
import { SharedModule } from '../shared/shared.module';
import { ControlePedidoRoutingModule } from './controlepedido-routing.module';
import { ControlePedidosCadastroComponent } from './controlepedidos-cadastro/controlepedido-cadastro.component';
import { ControlePedidosPesquisaComponent } from './controlepedidos-pesquisa/controlepedido-pesquisa.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    InputTextModule,
    ButtonModule,
    TableModule,
    TooltipModule,
    SelectButtonModule,
    InputMaskModule,
    MultiSelectModule,
    ToolbarModule,
    DropdownModule,
    
    SharedModule,
    ControlePedidoRoutingModule
  ],
  declarations: [
    // Declaração de quais Componentes(HTML) Servidor por Esse Módulo
    ControlePedidosCadastroComponent,
    ControlePedidosPesquisaComponent
  ],
  exports: []
})
export class ControlePedidoModule { }
