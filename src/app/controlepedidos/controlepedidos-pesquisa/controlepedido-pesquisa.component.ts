import { Component, OnInit } from '@angular/core';
import { ControlePedidoService } from '../controlepedido.service';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'app/seguranca/auth.service';
import { ConfirmationService } from 'primeng/api';
import { ErrorHandlerService } from 'app/core/error-handler.service';
import { ControlePedido } from '../../../app/model/controlepedido';

@Component({
  selector: 'app-controlepedido-pesquisa',
  templateUrl: './controlepedido-pesquisa.component.html',
  styleUrls: ['./controlepedido-pesquisa.component.css']
})
export class ControlePedidosPesquisaComponent implements OnInit {

  controlePedidos: ControlePedido[];
  cols: any[];

  constructor(
    private auth: AuthService,
    private controlePedidoService: ControlePedidoService,
    private toastr: ToastrService,
    private confirmation: ConfirmationService,
    private errorHandler: ErrorHandlerService
  ) { }

  ngOnInit() {
    this.pesquisar();
  }

  pesquisar() {
    this.controlePedidoService.pesquisarResumo().then(resultado => {
        this.controlePedidos = resultado
    }).catch(erro => this.errorHandler.handle(erro));
   
    this.cols = [
      { field: 'senha', header: 'Senha' },
      { field: 'nome', header: 'Nome' },
      { field: 'data', header: 'Data' },
      { field: 'statusControlePedido', header: 'Status' },
      { field: 'codigo', header: '' }
    ];
  }

  confirmarExcluir(controlePedido: any) {
    this.confirmation.confirm({
      message: 'Deseja Realmente Excluir esta Senha?',
      accept: () => {
        this.excluir(controlePedido);
      }
    });
  }

  excluir(controlePedido: any) {
    this.controlePedidoService.excluir(controlePedido.codigo).then(() => {
      this.pesquisar();
      this.toastr.success('Senha Excluída com Sucesso!');
    }).catch(erro => this.errorHandler.handle(erro));
  }

  alterarStatus(controlePedido: any): void {
    const novoStatus = !controlePedido.statusControlePedido;

    this.controlePedidoService.alterarStatus(controlePedido.codigo, novoStatus).then(() => {
      const acao = novoStatus ? 'Ativada' : 'Desativada';
      this.pesquisar();
      this.toastr.success(`Senha ${acao} com Sucesso!`);
    }).catch(erro => this.errorHandler.handle(erro));
  }
  
}
