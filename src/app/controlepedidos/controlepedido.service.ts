import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { SisHttp } from '../seguranca/sis-http';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { ControlePedido } from '../../app/model/controlepedido';

export interface ControlePedidoFiltro {
  statusControlePedido: string;
}

@Injectable()
export class ControlePedidoService {

  controlePedidosUrl: string;

  constructor(private http: SisHttp) {
    this.controlePedidosUrl = `${environment.apiUrl}/controlepedidos`;
  }

  pesquisar(filtro: ControlePedidoFiltro): Promise<any> {
    let params = new HttpParams();
    if (filtro.statusControlePedido) {
      params = params.set('statusControlePedido', filtro.statusControlePedido);
    }
    return this.http.get<any>(`${this.controlePedidosUrl}`, {params}).toPromise();
  }

  // Método de Pesquisar com a Paginação
  pesquisarResumo(): Promise<any> {
    return this.http.get<any>(`${this.controlePedidosUrl}?resumo`).toPromise();
  }

  listarControlePedidos(): Promise<any> {
    return this.http.get(`${this.controlePedidosUrl}`).toPromise();
  }

  // Método de Excluir
  excluir(codigo: number): Promise<void> {
    return this.http.delete<any>(`${this.controlePedidosUrl}/${codigo}`).toPromise();
  }

  // Método de Adicionar
  adicionar(controlePedido: ControlePedido): Promise<ControlePedido> {
    return this.http.post<ControlePedido>(this.controlePedidosUrl, controlePedido).toPromise();
  }

  // Método de Atualizar
  atualizar(controlePedido: ControlePedido): Promise<ControlePedido> {
    return this.http.put<ControlePedido>(`${this.controlePedidosUrl}/${controlePedido.codigo}`, controlePedido).toPromise();
  }

  // Método para Alterar a Propriedade Status
  alterarStatus(codigo: number, ativo: boolean): Promise<void> {
    const headers = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.put(`${this.controlePedidosUrl}/${codigo}/status`, ativo, { headers} ).toPromise().then(() => null);
  }

  buscarPorCodigo(codigo: number): Promise<ControlePedido> {
    return this.http.get<ControlePedido>(`${this.controlePedidosUrl}/${codigo}`).toPromise();
  }

}
