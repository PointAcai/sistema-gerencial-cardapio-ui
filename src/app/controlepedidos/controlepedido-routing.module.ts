import { AuthGuard } from '../seguranca/auth.guard';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { ControlePedidosCadastroComponent } from './controlepedidos-cadastro/controlepedido-cadastro.component';
import { ControlePedidosPesquisaComponent } from './controlepedidos-pesquisa/controlepedido-pesquisa.component';

const routes: Routes = [
  // canActivate é utilizado para Proteger os Links de Navegação
  {
    // Página de Pesquisa
    path: '',
    component: ControlePedidosPesquisaComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_PESQUISAR_CONTROLE_PEDIDOS'] }
  },
  {
    // Página de Cadastro
    path: 'cadastrar',
    component: ControlePedidosCadastroComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_CADASTRAR_CONTROLE_PEDIDOS'] }
  },
  {
    // Página de Alteração
    path: ':codigo',
    component: ControlePedidosCadastroComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_ALTERAR_CONTROLE_PEDIDOS'] }
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ControlePedidoRoutingModule { }
