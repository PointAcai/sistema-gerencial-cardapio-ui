import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationService } from '../../../../node_modules/primeng/components/common/api';
import { ErrorHandlerService } from './../../core/error-handler.service';
import { Categoria } from './../../model/categoria';
import { AuthService } from './../../seguranca/auth.service';
import { CategoriaService } from './../categoria.service';

@Component({
  selector: 'app-categoria-pesquisa',
  templateUrl: './categoria-pesquisa.component.html',
  styleUrls: ['./categoria-pesquisa.component.css']
})
export class CategoriaPesquisaComponent implements OnInit {

  categorias: Categoria[];
  cols: any[];

  constructor(
    private auth: AuthService,
    private categoriaService: CategoriaService,
    private toastr: ToastrService,
    private confirmation: ConfirmationService,
    private errorHandler: ErrorHandlerService,
  ) { }

  ngOnInit() {
    this.pesquisar();
  }

  pesquisar() {
    this.categoriaService.pesquisar().then(resultado => {
      this.categorias = resultado;
    }).catch(erro => this.errorHandler.handle(erro));

    this.cols = [
      { field: 'numIdentificador', header: 'Código' },
      { field: 'descricao', header: 'Descrição' },
      { field: 'statusCategoria', header: 'Status' },
      { field: 'codigo', header: '' }
    ];
  }

  confirmarExcluir(categoria: any) {
    this.confirmation.confirm({
      message: 'Deseja Realmente Excluir esta Categoria?',
      accept: () => {
        this.excluir(categoria);
      }
    });
  }

  excluir(categoria: any) {
    this.categoriaService.excluir(categoria.codigo).then(resultado => {
      if (resultado) {
        this.pesquisar();
        this.toastr.success('Categoria Excluída com Sucesso!');
      } else {
        this.toastr.error('Categoria não Excluída! Existem Produtos associados à essa Categoria.');
      }
    }).catch(erro => this.errorHandler.handle(erro));
  }

  alterarStatus(categoria: any): void {
    const novoStatus = !categoria.statusCategoria;

    this.categoriaService.alterarStatus(categoria.codigo, novoStatus)
      .then(() => {
        const acao = novoStatus ? 'Ativada' : 'Desativada';
        categoria.statusCategoria = novoStatus;
        this.toastr.success(`Categoria ${acao} com Sucesso!`);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

}
