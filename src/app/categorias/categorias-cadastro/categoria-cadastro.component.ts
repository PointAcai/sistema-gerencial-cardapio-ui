import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';

import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { ErrorHandlerService } from './../../core/error-handler.service';
import { CategoriaService } from './../categoria.service';
import { Categoria } from '../../model/categoria';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-categoria-cadastro',
  templateUrl: './categoria-cadastro.component.html',
  styleUrls: ['./categoria-cadastro.component.css'],
})
export class CategoriaCadastroComponent implements OnInit {

  formulario: FormGroup;

  constructor(
    private categoriaService: CategoriaService,
    private errorHandler: ErrorHandlerService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    // Configuração do Formulário Reativo
    this.configurarFormularioReativo();

    const codigoCategoria = this.route.snapshot.params['codigo'];
    if (codigoCategoria) {
      this.carregarCategoria(codigoCategoria);
    }
  }

  configurarFormularioReativo() {
    this.formulario = this.formBuilder.group({
      codigo: '',
      numIdentificador: ['', [ this.validarObrigatoriedade ]],
      descricao: [ '', [ this.validarObrigatoriedade ] ]
    });
  }

  validarObrigatoriedade(input: FormControl) {
    return (input.value ? null : { obrigatoriedade: true });
  }

  carregarCategoria(codigo: number) {
    this.categoriaService.buscarPorCodigo(codigo)
      .then(categoria => {
        this.formulario.patchValue(categoria);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  get editando() {
    return Boolean(this.formulario.get('codigo').value);
  }

  salvar() {
    if (this.editando) {
      this.atualizarCategoria();
    } else {
      this.adicionarCategoria();
    }
  }

  adicionarCategoria() {
    this.categoriaService.adicionar(this.formulario.value)
      .then(categoriaAdicionado => {
        this.toastr.success('Categoria Adicionado com Sucesso!');
        this.router.navigate(['/categorias']);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  atualizarCategoria() {
    this.categoriaService.atualizar(this.formulario.value)
      .then(categoria => {
        this.formulario.patchValue(categoria);
        this.toastr.success('Categoria Atualizado com Sucesso!');
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  novo() {
    this.formulario.reset();
    setTimeout(function() {
      this.categoria = new Categoria();
    }.bind(this), 1);
    this.router.navigate(['/categorias/cadastrar']);
  }

}
