import { AuthGuard } from './../seguranca/auth.guard';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { CategoriaCadastroComponent } from './categorias-cadastro/categoria-cadastro.component';
import { CategoriaPesquisaComponent } from './categorias-pesquisa/categoria-pesquisa.component';

const routes: Routes = [
  // canActivate é utilizado para Proteger os Links de Navegação
  {
    // Página de Pesquisa
    path: '',
    component: CategoriaPesquisaComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_PESQUISAR_CATEGORIA'] }
  },
  {
    // Página de Cadastro
    path: 'cadastrar',
    component: CategoriaCadastroComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_CADASTRAR_CATEGORIA'] }
  },
  {
    // Página de Alteração
    path: ':codigo',
    component: CategoriaCadastroComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_ALTERAR_CATEGORIA'] }
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class CategoriaRoutingModule { }
