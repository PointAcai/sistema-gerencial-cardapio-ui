import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { InputMaskModule } from 'primeng/inputmask';
import { InputTextModule } from 'primeng/inputtext';
import { MultiSelectModule } from 'primeng/multiselect';
import { SelectButtonModule } from 'primeng/selectbutton';
import { SpinnerModule } from 'primeng/spinner';
import { TableModule } from 'primeng/table';
import { ToolbarModule } from 'primeng/toolbar';
import { TooltipModule } from 'primeng/tooltip';
import { SharedModule } from './../shared/shared.module';
import { CategoriaRoutingModule } from './categoria-routing.module';
import { CategoriaCadastroComponent } from './categorias-cadastro/categoria-cadastro.component';
import { CategoriaPesquisaComponent } from './categorias-pesquisa/categoria-pesquisa.component';




@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    InputTextModule,
    ButtonModule,
    TableModule,
    TooltipModule,
    SelectButtonModule,
    InputMaskModule,
    MultiSelectModule,
    SpinnerModule,
    ToolbarModule,

    SharedModule,
    CategoriaRoutingModule
  ],
  declarations: [
    // Declaração de quais Componentes(HTML) Servidor por Esse Módulo
    CategoriaCadastroComponent,
    CategoriaPesquisaComponent
  ],
  exports: []
})
export class CategoriaModule { }
