import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder, Validators, FormControl } from '../../../../node_modules/@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '../../../../node_modules/@angular/router';

import { ErrorHandlerService } from './../../core/error-handler.service';
import { PerfilService } from './../perfil.service';
import { Perfil } from '../../model/perfil';
import { PermissaoService } from '../../perfis/permissao.service';
import { SelectItem } from 'primeng/api';
import { Permissao } from '../../model/permissao';

@Component({
  selector: 'app-perfil-cadastro',
  templateUrl: './perfil-cadastro.component.html',
  styleUrls: ['./perfil-cadastro.component.css'],
})
export class PerfilCadastroComponent implements OnInit {

  formulario: FormGroup;

  permissoesBase: SelectItem[];
  permissoes: Permissao[];

  constructor(
    private perfilService: PerfilService,
    private permissaoService: PermissaoService,
    private errorHandler: ErrorHandlerService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    // Configuração do Formulário Reativo
    this.configurarFormularioReativo();

    // Carrea Todas as Permissões
    this.carregarPermissoes();

    const codigoPerfil = this.route.snapshot.params['codigo'];
    if (codigoPerfil) {
      this.carregarPerfil(codigoPerfil);
    }
  }

  configurarFormularioReativo() {
    this.formulario = this.formBuilder.group({
      codigo: '',
      descricao: [ '', [ this.validarObrigatoriedade, this.validarTamanhoMinimo(5) ] ],
      permissoes: [[], Validators.required]
    });
  }

  validarObrigatoriedade(input: FormControl) {
    return (input.value ? null : { obrigatoriedade: true });
  }

  validarTamanhoMinimo(valor: number) {
    return (input: FormControl) => {
      return (!input.value || input.value.length >= valor) ? null : { tamanhoMinimo: { tamanho: valor } };
    };
  }

  carregarPerfil(codigo: number) {
    this.perfilService.buscarPorCodigo(codigo)
      .then(perfil => {
        this.formulario.patchValue(perfil);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  carregarPermissoes() {
    this.permissaoService.listarPermissoes()
      .then(permissoes => {
        this.permissoes = permissoes;
        this.permissoesBase = permissoes;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  get editando() {
    return Boolean(this.formulario.get('codigo').value);
  }

  salvar() {
    if (this.editando) {
      this.atualizarPerfil();
    } else {
      this.adicionarPerfil();
    }
  }

  adicionarPerfil() {
    this.perfilService.adicionar(this.formulario.value)
      .then(perfilAdicionado => {
        this.toastr.success('Perfil Adicionado com Sucesso!');
        this.router.navigate(['/perfis']);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  atualizarPerfil() {
    this.perfilService.atualizar(this.formulario.value)
      .then(perfil => {
        this.formulario.patchValue(perfil);
        this.toastr.success('Perfil Atualizado com Sucesso!');
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  novo() {
    this.formulario.reset();
    setTimeout(function() {
      this.perfil = new Perfil();
    }.bind(this), 1);
    this.router.navigate(['/perfis/cadastrar']);
  }

}
