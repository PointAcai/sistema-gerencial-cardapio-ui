import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Perfil } from '../model/perfil';
import { environment } from './../../environments/environment';
import { SisHttp } from './../seguranca/sis-http';

@Injectable()
export class PerfilService {

  perfilUrl: string;

  constructor(private http: SisHttp) {
    this.perfilUrl = `${environment.apiUrl}/perfis`;
  }

  // Método de Pesquisar com a Paginação
  pesquisar(): Promise<any> {
    return this.http.get<any>(`${this.perfilUrl}?resumo`).toPromise();
  }

  // Método de Excluir
  excluir(codigo: number): Promise<void> {
    return this.http.delete<any>(`${this.perfilUrl}/${codigo}`).toPromise();
  }

  // Método de Adicionar
  adicionar(perfil: Perfil): Promise<Perfil> {
    return this.http.post<Perfil>(this.perfilUrl, perfil).toPromise();
  }

  // Método de Atualizar
  atualizar(perfil: Perfil): Promise<Perfil> {
    return this.http.put<Perfil>(`${this.perfilUrl}/${perfil.codigo}`, perfil).toPromise();
  }

  // Método para Alterar a Propriedade Status
  alterarStatus(codigo: number, ativo: boolean): Promise<void> {
    const headers = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.put(`${this.perfilUrl}/${codigo}/status`, ativo, { headers} ).toPromise().then(() => null);
  }

  // Carregar a Combo com Todos os Perfis ATIVOS
  listarPerfis(): Promise<any> {
    // Filtrar por Status Ativo
    const params = new HttpParams().append('status', 'true');
    return this.http.get(`${this.perfilUrl}`,  { params }).toPromise();
  }

  buscarPorCodigo(codigo: number): Promise<Perfil> {
    return this.http.get<Perfil>(`${this.perfilUrl}/${codigo}`).toPromise();
  }

}
