import { ToolbarModule } from 'primeng/toolbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SelectButtonModule } from 'primeng/selectbutton';
import { TooltipModule } from 'primeng/tooltip';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { InputMaskModule } from 'primeng/inputmask';
import { MultiSelectModule } from 'primeng/multiselect';

import { SharedModule } from './../shared/shared.module';
import { PerfilRoutingModule } from './perfil-routing.module';

import { PerfilPesquisaComponent } from './perfis-pesquisa/perfil-pesquisa.component';
import { PerfilCadastroComponent } from './perfis-cadastro/perfil-cadastro.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    InputTextModule,
    ButtonModule,
    TableModule,
    TooltipModule,
    SelectButtonModule,
    InputMaskModule,
    MultiSelectModule,
    ToolbarModule,

    SharedModule,
    PerfilRoutingModule
  ],
  declarations: [
    // Declaração de quais Componentes(HTML) Servidor por Esse Módulo
    PerfilCadastroComponent,
    PerfilPesquisaComponent
  ],
  exports: []
})
export class PerfilModule { }
