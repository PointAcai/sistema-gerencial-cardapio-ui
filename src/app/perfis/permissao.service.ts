import { SisHttp } from './../seguranca/sis-http';

import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class PermissaoService {

  permissaoUrl: string;

  constructor(private http: SisHttp) {
    this.permissaoUrl = `${environment.apiUrl}/permissoes`;
  }

  listarPermissoes(): Promise<any> {
    return this.http.get(`${this.permissaoUrl}`, { }).toPromise();
  }

}
