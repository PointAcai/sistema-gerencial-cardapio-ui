import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationService } from '../../../../node_modules/primeng/components/common/api';
import { ErrorHandlerService } from './../../core/error-handler.service';
import { Perfil } from './../../model/perfil';
import { AuthService } from './../../seguranca/auth.service';
import { PerfilService } from './../perfil.service';

@Component({
  selector: 'app-perfil-pesquisa',
  templateUrl: './perfil-pesquisa.component.html',
  styleUrls: ['./perfil-pesquisa.component.css']
})
export class PerfilPesquisaComponent implements OnInit {

  perfis: Perfil[];
  cols: any[];

  constructor(
    private auth: AuthService,
    private perfilService: PerfilService,
    private toastr: ToastrService,
    private confirmation: ConfirmationService,
    private errorHandler: ErrorHandlerService,
  ) { }

  ngOnInit() {
    this.pesquisar();
  }

  pesquisar() {
    this.perfilService.pesquisar().then(resultado => {
      this.perfis = resultado;
    }).catch(erro => this.errorHandler.handle(erro));

    this.cols = [
      { field: 'codigo', header: 'Código' },
      { field: 'descricao', header: 'Descrição' },
      { field: 'status', header: 'Status' },
      { field: 'codigo', header: '' }
    ];
  }

  confirmarExcluir(perfil: any) {
    this.confirmation.confirm({
      message: 'Deseja Realmente Excluir este Perfil?',
      accept: () => {
        this.excluir(perfil);
      }
    });
  }

  excluir(perfil: any) {
    this.perfilService.excluir(perfil.codigo).then(() => {
      this.pesquisar();
      this.toastr.success('Perfil Excluído com Sucesso!');
    }).catch(erro => this.errorHandler.handle(erro));
  }

  alterarStatus(perfil: any): void {
    const novoStatus = !perfil.status;

    this.perfilService.alterarStatus(perfil.codigo, novoStatus)
      .then(() => {
        const acao = novoStatus ? 'Ativado' : 'Desativado';
        perfil.status = novoStatus;
        this.toastr.success(`Perfil ${acao} com Sucesso!`);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

}
