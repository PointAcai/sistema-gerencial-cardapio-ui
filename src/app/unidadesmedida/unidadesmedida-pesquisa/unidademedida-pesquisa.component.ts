import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationService } from '../../../../node_modules/primeng/components/common/api';
import { ErrorHandlerService } from './../../core/error-handler.service';
import { UnidadeMedida } from './../../model/unidademedida';
import { AuthService } from './../../seguranca/auth.service';
import { UnidadeMedidaService } from './../unidademedida.service';

@Component({
  selector: 'app-unidademedida-pesquisa',
  templateUrl: './unidademedida-pesquisa.component.html',
  styleUrls: ['./unidademedida-pesquisa.component.css']
})
export class UnidadeMedidaPesquisaComponent implements OnInit {

  unidadesmedida: UnidadeMedida[];
  cols: any[];

  constructor(
    private auth: AuthService,
    private unidadeMedidaService: UnidadeMedidaService,
    private toastr: ToastrService,
    private confirmation: ConfirmationService,
    private errorHandler: ErrorHandlerService,
  ) { }

  ngOnInit() {
    this.pesquisar();
  }

  pesquisar() {
    this.unidadeMedidaService.pesquisar().then(resultado => {
        this.unidadesmedida = resultado
    }).catch(erro => this.errorHandler.handle(erro));

    this.cols = [
      { field: 'codigo', header: 'Código' },
      { field: 'sigla', header: 'Sigla' },
      { field: 'descricao', header: 'Descrição' },
      { field: 'status', header: 'Status' },
      { field: 'codigo', header: '' }
    ];
  }

  confirmarExcluir(unidadeMedida: any) {
    this.confirmation.confirm({
      message: 'Deseja Realmente Excluir esta Unidade de Medida?',
      accept: () => {
        this.excluir(unidadeMedida);
      }
    });
  }

  excluir(unidadeMedida: any) {
    this.unidadeMedidaService.excluir(unidadeMedida.codigo).then(() => {
      this.pesquisar();
      this.toastr.success('Unidade de Medida Excluído com Sucesso!');
    }).catch(erro => this.errorHandler.handle(erro));
  }

  alterarStatus(unidadeMedida: any): void {
    const novoStatus = !unidadeMedida.status;

    this.unidadeMedidaService.alterarStatus(unidadeMedida.codigo, novoStatus).then(() => {
      const acao = novoStatus ? 'Ativado' : 'Desativado';
      unidadeMedida.status = novoStatus;
      this.toastr.success(`Unidade de Medida ${acao} com Sucesso!`);
    }).catch(erro => this.errorHandler.handle(erro));
  }

}
