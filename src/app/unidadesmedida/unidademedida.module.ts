import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { InputMaskModule } from 'primeng/inputmask';
import { InputTextModule } from 'primeng/inputtext';
import { MultiSelectModule } from 'primeng/multiselect';
import { SelectButtonModule } from 'primeng/selectbutton';
import { TableModule } from 'primeng/table';
import { ToolbarModule } from 'primeng/toolbar';
import { TooltipModule } from 'primeng/tooltip';
import { SharedModule } from './../shared/shared.module';
import { UnidadeMedidaRoutingModule } from './unidademedida-routing.module';
import { UnidadeMedidaCadastroComponent } from './unidadesmedida-cadastro/unidademedida-cadastro.component';
import { UnidadeMedidaPesquisaComponent } from './unidadesmedida-pesquisa/unidademedida-pesquisa.component';




@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    InputTextModule,
    ButtonModule,
    TableModule,
    TooltipModule,
    SelectButtonModule,
    InputMaskModule,
    MultiSelectModule,
    ToolbarModule,

    SharedModule,
    UnidadeMedidaRoutingModule
  ],
  declarations: [
    // Declaração de quais Componentes(HTML) Servidor por Esse Módulo
    UnidadeMedidaCadastroComponent,
    UnidadeMedidaPesquisaComponent
  ],
  exports: []
})
export class UnidadeMedidaModule { }
