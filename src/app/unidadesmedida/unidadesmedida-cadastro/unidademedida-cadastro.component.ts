import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder, FormControl } from '../../../../node_modules/@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '../../../../node_modules/@angular/router';

import { ErrorHandlerService } from './../../core/error-handler.service';
import { UnidadeMedidaService } from './../unidademedida.service';
import { UnidadeMedida } from '../../model/unidademedida';

@Component({
  selector: 'app-unidademedida-cadastro',
  templateUrl: './unidademedida-cadastro.component.html',
  styleUrls: ['./unidademedida-cadastro.component.css'],
})
export class UnidadeMedidaCadastroComponent implements OnInit {

  formulario: FormGroup;

  constructor(
    private unidadeMedidaService: UnidadeMedidaService,
    private errorHandler: ErrorHandlerService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    // Configuração do Formulário Reativo
    this.configurarFormularioReativo();

    const codigoUnidadeMedida = this.route.snapshot.params['codigo'];
    if (codigoUnidadeMedida) {
      this.carregarUnidadeMedida(codigoUnidadeMedida);
    }
  }

  configurarFormularioReativo() {
    this.formulario = this.formBuilder.group({
      codigo: '',
      descricao: [ '', [ this.validarObrigatoriedade ] ],
      sigla: [ '', [ this.validarObrigatoriedade ] ]
    });
  }

  validarObrigatoriedade(input: FormControl) {
    return (input.value ? null : { obrigatoriedade: true });
  }

  carregarUnidadeMedida(codigo: number) {
    this.unidadeMedidaService.buscarPorCodigo(codigo)
      .then(unidadeMedida => {
        this.formulario.patchValue(unidadeMedida);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  get editando() {
    return Boolean(this.formulario.get('codigo').value);
  }

  salvar() {
    if (this.editando) {
      this.atualizarUnidadeMedida();
    } else {
      this.adicionarUnidadeMedida();
    }
  }

  adicionarUnidadeMedida() {
    this.unidadeMedidaService.adicionar(this.formulario.value)
      .then(unidadeMedidaAdicionado => {
        this.toastr.success('UnidadeMedida Adicionado com Sucesso!');
        this.router.navigate(['/unidadesmedida']);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  atualizarUnidadeMedida() {
    this.unidadeMedidaService.atualizar(this.formulario.value)
      .then(unidadeMedida => {
        this.formulario.patchValue(unidadeMedida);
        this.toastr.success('UnidadeMedida Atualizado com Sucesso!');
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  novo() {
    this.formulario.reset();
    setTimeout(function() {
      this.unidadeMedida = new UnidadeMedida();
    }.bind(this), 1);
    this.router.navigate(['/perfis/cadastrar']);
  }

}
