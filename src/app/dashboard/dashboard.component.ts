import { SelectItem } from 'primeng/api';
import { AuthService } from './../seguranca/auth.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {

  categorias: SelectItem[];

  constructor(
    public auth: AuthService,
    private toastr: ToastrService
  ) { }


}
