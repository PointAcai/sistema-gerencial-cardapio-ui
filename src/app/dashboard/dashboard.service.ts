import { SisHttp } from './../seguranca/sis-http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class DashboardService {

  dashboardUrl: string;

  constructor(private http: SisHttp) {
    this.dashboardUrl = `${environment.apiUrl}/dashboard`;
  }

}
