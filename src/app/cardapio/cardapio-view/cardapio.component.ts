import { CardapioService } from './../cardapio.service';
import { Cardapio } from './../../model/cardapio';
import { Component, OnInit } from '@angular/core';
import { ErrorHandlerService } from './../../core/error-handler.service';
import { Produto } from 'app/model/produto';
import { OverlayPanel } from 'primeng/overlaypanel';
import { ProdutoService } from 'app/produtos/produto.service';
import { ParametrosService } from 'app/parametros/parametro.service';

const CHAVE_URL_LOGO = 'LOGO_CARDAPIO';

@Component({
  selector: 'app-cardapio',
  templateUrl: './cardapio.component.html',
  styleUrls: ['./cardapio.component.css']
})
export class CardapioComponent implements OnInit {

  urlBackground: any;

  cardapio: Array<Cardapio> = [];

  produto: Produto;

  constructor(
    private cardapioService: CardapioService,
    private produtoService: ProdutoService,
    private parametrosService: ParametrosService,
    private errorHandler: ErrorHandlerService
  ) { }

  ngOnInit() {
    // this.pesquisarPorcoesEMolhos();
    this.listarCategoriasCardapio();
    this.getUrl();
  }

  listarCategoriasCardapio() {
    this.cardapioService.listarCategoriasCardapio().then(cardapio => {
        const cardapioConfigurado: Array<Cardapio> = [];

        this.configurarCardapio(cardapio, cardapioConfigurado);

        if (cardapio.ofertas.length > 1) {
          for (let index = 0; index < cardapio.ofertas.length; index++) {
            cardapioConfigurado.push({tipo : 'oferta', conteudo : cardapio.ofertas[index]});
            if (index + 1 !== cardapio.ofertas.length) {
              this.configurarCardapio(cardapio, cardapioConfigurado);
            }
          }
        } else if (cardapio.ofertas.length === 1) {
          cardapioConfigurado.push({tipo : 'oferta', conteudo : cardapio.ofertas[0]});
        }

        this.cardapio = cardapioConfigurado;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  configurarCardapio(cardapio, cardapioConfigurado) {
    if (cardapio.categorias.length > 4) {
      const itensCardapio1:  Array<any> = [];
      for (let index = 0; index <= 2; index++) {
        itensCardapio1.push(cardapio.categorias[index]);
      }
      cardapioConfigurado.push({tipo : 'cardapio', conteudo : {cardapio : itensCardapio1}});

      const itensCardapio2:  Array<any> = [];
      for (let index = 4; index < cardapio.categorias.length; index++) {
        itensCardapio2.push(cardapio.categorias[index]);
      }
      cardapioConfigurado.push({tipo : 'cardapio', conteudo : {cardapio : itensCardapio2}});

    } else {
      cardapioConfigurado.push({tipo : 'cardapio', conteudo : {cardapio : cardapio.categorias}});
    }
  }

  selecionarImagem(event, produto: Produto, overlaypanel: OverlayPanel) {
    this.produto = produto;
    overlaypanel.toggle(event);
  }

  getUrl() {
    this.parametrosService.pesquisar({ chave: CHAVE_URL_LOGO }).then(resultado => {
      this.urlBackground = resultado.content[0].urlImagem;
    }).catch(erro => this.errorHandler.handle(erro));
  }

  getStyle() {
    return "transparent url('"+this.urlBackground+"') center center /cover";
  }

}
