import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from './../shared/shared.module';
import { CardapioRoutingModule } from './cardapio-routing.module';

import { CardapioComponent } from './cardapio-view/cardapio.component';
import { CarouselModule } from 'primeng/carousel';
import { TableModule } from 'primeng/table';
import {CardModule} from 'primeng/card';
import {ButtonModule} from 'primeng/button';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import {FieldsetModule} from 'primeng/fieldset';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CarouselModule,
    TableModule,
    CardModule,
    ButtonModule,
    OverlayPanelModule,
    FieldsetModule,

    SharedModule,
    CardapioRoutingModule
  ],
  declarations: [
    CardapioComponent
  ],
  exports: []
})
export class CardapioModule { }
