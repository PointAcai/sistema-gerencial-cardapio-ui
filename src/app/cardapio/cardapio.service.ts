import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';

@Injectable()
export class CardapioService {

  cardapioUrl: string;

  constructor(
    private http: HttpClient
  ) {
    this.cardapioUrl = `${environment.apiUrl}/cardapio`;
  }

  listarCategoriasCardapio(): Promise<any> {
    const headers = new HttpHeaders()
    .append('Content-Type', 'application/x-www-form-urlencoded')
    // Esse Authorization é a Autorização da API e não do Usuário!
    .append('Authorization', 'Basic YW5ndWxhcjpAbmd1bEByMA==');

    return this.http.get(`${this.cardapioUrl}`, { headers, withCredentials: true }).toPromise();
  }

}
