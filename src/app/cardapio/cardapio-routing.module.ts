import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { CardapioComponent } from './cardapio-view/cardapio.component';

const routes: Routes = [
  // canActivate é utilizado para Proteger os Links de Navegação
  {
    // Página de Pesquisa
    path: '',
    component: CardapioComponent
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class CardapioRoutingModule { }
