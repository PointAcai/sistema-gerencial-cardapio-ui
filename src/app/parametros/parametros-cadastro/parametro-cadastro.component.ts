import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';

import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';

import { ErrorHandlerService } from '../../core/error-handler.service';
import { ParametrosService } from '../parametro.service';
import { SelectItem } from 'primeng/api';
import { Parametro } from '../../model/parametro';

@Component({
  selector: 'app-parametro-cadastro',
  templateUrl: './parametro-cadastro.component.html',
  styleUrls: ['./parametro-cadastro.component.css'],
})
export class ParametrosCadastroComponent implements OnInit {

  formulario: FormGroup;
  uploadEmAndamento: Boolean = false;

  tipos: SelectItem[];

  constructor(
    private parametrosService: ParametrosService,
    private errorHandler: ErrorHandlerService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.configurarFormularioReativo();

    this.listarTipos();

    const codigoParametro = this.route.snapshot.params['codigo'];
    if (codigoParametro) {
      this.carregarProduto(codigoParametro);
    }

  }

  configurarFormularioReativo() {
    this.formulario = this.formBuilder.group({
      codigo: null,
      descricao: ['', [ this.validarObrigatoriedade ]],
      chave: ['', [ this.validarObrigatoriedade ]],
      valor: ['', [ this.validarObrigatoriedade ]],
      tipo: ['', [ this.validarObrigatoriedade ]],
      urlImagem: []
    });
  }

  listarTipos() {
    this.tipos = [
      {label: "Selecione um Tipo", value: null},
      {label: "Texto", value: "Texto"},
      {label: "Imagem", value: "Imagem"}
    ];
  }

  /**
   * S3
   */

  antesUploadImagem(event) {
    event.xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'));
    this.uploadEmAndamento = true;
  }

  aoTerminarUploadImagem(event) {
    const imagem = JSON.parse(event.xhr.response);
    this.formulario.patchValue({
      valor: imagem.nome,
      urlImagem: imagem.url
    });
    this.uploadEmAndamento = false;
  }

  erroUpload(event) {
    this.toastr.error('Erro ao Tentar enviar Imagem do Parâmetro.');
    this.uploadEmAndamento = false;
  }

  removerImagem() {
    this.formulario.patchValue({
      valor: null,
      urlImagem: null
    });
  }

  get nomeAnexo() {
    const nome = this.formulario.get('valor').value;
    if (nome) {
      return nome.substring(nome.indexOf('_') + 1, nome.length);
    }
    return '';
  }

  get urlUploadImagem() {
    return this.parametrosService.urlUploadImagem();
  }

  verificarTipoSelecionado() {
    const tipo = this.formulario.get('tipo').value;
    const valor = this.formulario.get('valor').value
    if (tipo == 'Texto' && valor) {
      this.removerImagem();
    } else {
      this.formulario.patchValue({
        valor: null
      });
    }
  }

  /**
   * VALIDAÇÕES
   */

  validarObrigatoriedade(input: FormControl) {
    return (input.value ? null : { obrigatoriedade: true });
  }

  /**
   * SERVIÇOS
   */

  carregarProduto(codigo: number) {
    this.parametrosService.buscarPorCodigo(codigo)
      .then(parametro => {
        this.formulario.patchValue(parametro);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  get editando() {
    return Boolean(this.formulario.get('codigo').value);
  }

  salvar() {
    if (this.editando) {
      this.atualizarParametro();
    } else {
      this.adicionarParametro();
    }
  }

  adicionarParametro() {
    this.parametrosService.adicionar(this.formulario.value)
      .then(parametroAdicionado => {
        this.toastr.success('Parâmetro Adicionado com Sucesso!');
        this.router.navigate(['/parametros']);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  atualizarParametro() {
    this.parametrosService.atualizar(this.formulario.value)
      .then(parametro => {
        this.formulario.patchValue(parametro);
        this.toastr.success('Parâmetro Atualizado com Sucesso!');
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  novo() {
    this.formulario.reset();
    setTimeout(function() {
      this.parametro = new Parametro;
    }.bind(this), 1);
    this.router.navigate(['/parametros/cadastrar']);
  }

}
