import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { SisHttp } from '../seguranca/sis-http';
import { Parametro } from 'app/model/parametro';
import { HttpHeaders, HttpParams } from '@angular/common/http';

export interface ParametroFiltro {
  chave: string;
}

@Injectable()
export class ParametrosService {

  parametrosUrl: string;

  constructor(private http: SisHttp) {
    this.parametrosUrl = `${environment.apiUrl}/parametros`;
  }

  urlUploadImagem(): string {
    return `${this.parametrosUrl}/imagem`
  }

  pesquisar(filtro: ParametroFiltro): Promise<any> {
    let params = new HttpParams();
    if (filtro.chave) {
      params = params.set('chave', filtro.chave);
    }
    return this.http.get<any>(`${this.parametrosUrl}`, {params}).toPromise();
  }

  // Método de Pesquisar com a Paginação
  pesquisarResumo(): Promise<any> {
    return this.http.get<any>(`${this.parametrosUrl}?resumo`).toPromise();
  }

  listarParametros(): Promise<any> {
    return this.http.get(`${this.parametrosUrl}`).toPromise();
  }

  // Método de Excluir
  excluir(codigo: number): Promise<void> {
    return this.http.delete<any>(`${this.parametrosUrl}/${codigo}`).toPromise();
  }

  // Método de Adicionar
  adicionar(parametro: Parametro): Promise<Parametro> {
    return this.http.post<Parametro>(this.parametrosUrl, parametro).toPromise();
  }

  // Método de Atualizar
  atualizar(parametro: Parametro): Promise<Parametro> {
    return this.http.put<Parametro>(`${this.parametrosUrl}/${parametro.codigo}`, parametro).toPromise();
  }

  // Método para Alterar a Propriedade Status
  alterarStatus(codigo: number, ativo: boolean): Promise<void> {
    const headers = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.put(`${this.parametrosUrl}/${codigo}/status`, ativo, { headers} ).toPromise().then(() => null);
  }

  buscarPorCodigo(codigo: number): Promise<Parametro> {
    return this.http.get<Parametro>(`${this.parametrosUrl}/${codigo}`).toPromise();
  }

}
