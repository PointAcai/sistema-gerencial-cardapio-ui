import { Component, OnInit } from '@angular/core';
import { ParametrosService } from '../parametro.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'app/seguranca/auth.service';
import { ConfirmationService } from 'primeng/api';
import { ErrorHandlerService } from 'app/core/error-handler.service';
import { Parametro } from 'app/model/parametro';

@Component({
  selector: 'app-parametro-pesquisa',
  templateUrl: './parametro-pesquisa.component.html',
  styleUrls: ['./parametro-pesquisa.component.css']
})
export class ParametrosPesquisaComponent implements OnInit {

  parametros: Parametro[];
  cols: any[];

  constructor(
    private auth: AuthService,
    private parametrosService: ParametrosService,
    private toastr: ToastrService,
    private confirmation: ConfirmationService,
    private errorHandler: ErrorHandlerService
  ) { }

  ngOnInit() {
    this.pesquisar();
  }

  pesquisar() {
    this.parametrosService.pesquisarResumo().then(resultado => {
        this.parametros = resultado
    }).catch(erro => this.errorHandler.handle(erro));

    this.cols = [
      { field: 'codigo', header: 'Código' },
      { field: 'descricao', header: 'Descrição' },
      { field: 'chave', header: 'Chave' },
      { field: 'tipo', header: 'Tipo' },
      { field: 'status', header: 'Status' },
      { field: 'codigo', header: '' }
    ];
  }

  confirmarExcluir(parametro: any) {
    this.confirmation.confirm({
      message: 'Deseja Realmente Excluir este Parâmetro?',
      accept: () => {
        this.excluir(parametro);
      }
    });
  }

  excluir(parametro: any) {
    this.parametrosService.excluir(parametro.codigo).then(() => {
      this.pesquisar();
      this.toastr.success('Parâmetro Excluído com Sucesso!');
    }).catch(erro => this.errorHandler.handle(erro));
  }

  alterarStatus(parametro: any): void {
    const novoStatus = !parametro.status;

    this.parametrosService.alterarStatus(parametro.codigo, novoStatus).then(() => {
      const acao = novoStatus ? 'Ativado' : 'Desativado';
      parametro.status = novoStatus;
      this.toastr.success(`Parâmetro ${acao} com Sucesso!`);
    }).catch(erro => this.errorHandler.handle(erro));
  }
  
}
