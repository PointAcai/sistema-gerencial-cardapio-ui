import { AuthGuard } from '../seguranca/auth.guard';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { ParametrosCadastroComponent } from './parametros-cadastro/parametro-cadastro.component';
import { ParametrosPesquisaComponent } from './parametros-pesquisa/parametro-pesquisa.component';

const routes: Routes = [
  // canActivate é utilizado para Proteger os Links de Navegação
  {
    // Página de Pesquisa
    path: '',
    component: ParametrosPesquisaComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_PESQUISAR_PARAMETROS'] }
  },
  {
    // Página de Cadastro
    path: 'cadastrar',
    component: ParametrosCadastroComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_CADASTRAR_PARAMETROS'] }
  },
  {
    // Página de Alteração
    path: ':codigo',
    component: ParametrosCadastroComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_ALTERAR_PARAMETROS'] }
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ParametroRoutingModule { }
