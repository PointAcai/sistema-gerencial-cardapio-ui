import { Component, OnInit } from '@angular/core';
import { Ingrediente } from 'app/model/ingrediente';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationService } from '../../../../node_modules/primeng/components/common/api';
import { ErrorHandlerService } from './../../core/error-handler.service';
import { AuthService } from './../../seguranca/auth.service';
import { IngredienteService } from './../ingrediente.service';

@Component({
  selector: 'app-ingrediente-pesquisa',
  templateUrl: './ingrediente-pesquisa.component.html',
  styleUrls: ['./ingrediente-pesquisa.component.css']
})
export class IngredientePesquisaComponent implements OnInit {

  ingredientes: Ingrediente[];
  cols: any[];

  constructor(
    private auth: AuthService,
    private ingredienteService: IngredienteService,
    private toastr: ToastrService,
    private confirmation: ConfirmationService,
    private errorHandler: ErrorHandlerService,
  ) { }

  ngOnInit() {
    this.pesquisar();
  }

  pesquisar() {
    this.ingredienteService.pesquisar().then(resultado => {
      this.ingredientes = resultado;
    }).catch(erro => this.errorHandler.handle(erro));

    this.cols = [
      { field: 'codigo', header: 'Código' },
      { field: 'descricao', header: 'Descrição' },
      { field: 'statusIngrediente', header: 'Status' },
      { field: 'codigo', header: '' }
    ];
  }

  confirmarExcluir(ingrediente: any) {
    this.confirmation.confirm({
      message: 'Deseja Realmente Excluir este Ingrediente?',
      accept: () => {
        this.excluir(ingrediente);
      }
    });
  }

  excluir(ingrediente: any) {
    this.ingredienteService.excluir(ingrediente.codigo).then(() => {
      this.pesquisar();
      this.toastr.success('Ingrediente Excluído com Sucesso!');
    }).catch(erro => this.errorHandler.handle(erro));
  }

  alterarStatus(ingrediente: any): void {
    const novoStatus = !ingrediente.statusIngrediente;

    this.ingredienteService.alterarStatus(ingrediente.codigo, novoStatus)
      .then(() => {
        const acao = novoStatus ? 'Ativado' : 'Desativado';
        ingrediente.statusIngrediente = novoStatus;
        this.toastr.success(`Ingrediente ${acao} com Sucesso!`);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

}
