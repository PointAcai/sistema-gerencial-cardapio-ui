import { ToolbarModule } from 'primeng/toolbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SelectButtonModule } from 'primeng/selectbutton';
import { TooltipModule } from 'primeng/tooltip';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { InputMaskModule } from 'primeng/inputmask';
import { MultiSelectModule } from 'primeng/multiselect';
import { RadioButtonModule } from 'primeng/radiobutton';
import { CurrencyMaskModule } from 'ng2-currency-mask';

import { SharedModule } from './../shared/shared.module';
import { IngredienteRoutingModule } from './ingrediente-routing.module';

import { IngredientePesquisaComponent } from './ingredientes-pesquisa/ingrediente-pesquisa.component';
import { IngredienteCadastroComponent } from './ingredientes-cadastro/ingrediente-cadastro.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    InputTextModule,
    ButtonModule,
    TableModule,
    TooltipModule,
    SelectButtonModule,
    InputMaskModule,
    MultiSelectModule,
    RadioButtonModule,
    CurrencyMaskModule,
    ToolbarModule,

    SharedModule,
    IngredienteRoutingModule
  ],
  declarations: [
    // Declaração de quais Componentes(HTML) Servidor por Esse Módulo
    IngredienteCadastroComponent,
    IngredientePesquisaComponent
  ],
  exports: []
})
export class IngredienteModule { }
