import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '../../../../node_modules/@angular/forms';
import { ActivatedRoute, Router } from '../../../../node_modules/@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Ingrediente } from '../../model/ingrediente';
import { ErrorHandlerService } from './../../core/error-handler.service';
import { IngredienteService } from './../ingrediente.service';



@Component({
  selector: 'app-ingrediente-cadastro',
  templateUrl: './ingrediente-cadastro.component.html',
  styleUrls: ['./ingrediente-cadastro.component.css'],
})
export class IngredienteCadastroComponent implements OnInit {

  formulario: FormGroup;

  constructor(
    private ingredienteService: IngredienteService,
    private errorHandler: ErrorHandlerService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    // Configuração do Formulário Reativo
    this.configurarFormularioReativo();

    const codigoIngrediente = this.route.snapshot.params['codigo'];
    if (codigoIngrediente) {
      this.carregarIngrediente(codigoIngrediente);
    }
  }

  configurarFormularioReativo() {
    this.formulario = this.formBuilder.group({
      codigo: '',
      descricao: [ '', [ this.validarObrigatoriedade ] ]
    });
  }

  validarObrigatoriedade(input: FormControl) {
    return (input.value ? null : { obrigatoriedade: true });
  }

  carregarIngrediente(codigo: number) {
    this.ingredienteService.buscarPorCodigo(codigo)
      .then(ingrediente => {
        this.formulario.patchValue(ingrediente);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  get editando() {
    return Boolean(this.formulario.get('codigo').value);
  }

  salvar() {
    if (this.editando) {
      this.atualizarIngrediente();
    } else {
      this.adicionarIngrediente();
    }
  }

  adicionarIngrediente() {
    this.ingredienteService.adicionar(this.formulario.value)
      .then(ingredienteAdicionado => {
        this.toastr.success('Ingrediente Adicionado com Sucesso!');
        this.router.navigate(['/ingredientes']);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  atualizarIngrediente() {
    this.ingredienteService.atualizar(this.formulario.value)
      .then(ingrediente => {
        this.formulario.patchValue(ingrediente);
        this.toastr.success('Ingrediente Atualizado com Sucesso!');
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  novo() {
    this.formulario.reset();
    setTimeout(function() {
      this.ingrediente = new Ingrediente();
    }.bind(this), 1);
    this.router.navigate(['/ingredientes/cadastrar']);
  }

}
