import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Ingrediente } from '../model/ingrediente';
import { environment } from './../../environments/environment';
import { SisHttp } from './../seguranca/sis-http';

@Injectable()
export class IngredienteService {

  ingredienteUrl: string;

  constructor(private http: SisHttp) {
    this.ingredienteUrl = `${environment.apiUrl}/ingredientes`;
  }

  // Método de Pesquisar com a Paginação
  pesquisar(): Promise<any> {
    return this.http.get<any>(`${this.ingredienteUrl}?resumo`).toPromise();
  }

  listarIngredientes(): Promise<any> {
    return this.http.get(`${this.ingredienteUrl}`).toPromise();
  }

  // Método de Excluir
  excluir(codigo: number): Promise<void> {
    return this.http.delete<any>(`${this.ingredienteUrl}/${codigo}`).toPromise();
  }

  // Método de Adicionar
  adicionar(ingrediente: Ingrediente): Promise<Ingrediente> {
    return this.http.post<Ingrediente>(this.ingredienteUrl, ingrediente).toPromise();
  }

  // Método de Atualizar
  atualizar(ingrediente: Ingrediente): Promise<Ingrediente> {
    return this.http.put<Ingrediente>(`${this.ingredienteUrl}/${ingrediente.codigo}`, ingrediente).toPromise();
  }

  // Método para Alterar a Propriedade Status
  alterarStatus(codigo: number, ativo: boolean): Promise<void> {
    const headers = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.put(`${this.ingredienteUrl}/${codigo}/status`, ativo, { headers} ).toPromise().then(() => null);
  }

  // Carregar a Combo com Todos os Perfis ATIVOS
  listarPerfis(): Promise<any> {
    // Filtrar por Status Ativo
    const params = new HttpParams().append('status', 'true');
    return this.http.get(`${this.ingredienteUrl}`, { params }).toPromise();
  }

  buscarPorCodigo(codigo: number): Promise<Ingrediente> {
    return this.http.get<Ingrediente>(`${this.ingredienteUrl}/${codigo}`, { }).toPromise();
  }

}
