import { AuthGuard } from './../seguranca/auth.guard';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { IngredienteCadastroComponent } from './ingredientes-cadastro/ingrediente-cadastro.component';
import { IngredientePesquisaComponent } from './ingredientes-pesquisa/ingrediente-pesquisa.component';

const routes: Routes = [
  // canActivate é utilizado para Proteger os Links de Navegação
  {
    // Página de Pesquisa
    path: '',
    component: IngredientePesquisaComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_PESQUISAR_CATEGORIA'] }
  },
  {
    // Página de Cadastro
    path: 'cadastrar',
    component: IngredienteCadastroComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_CADASTRAR_CATEGORIA'] }
  },
  {
    // Página de Alteração
    path: ':codigo',
    component: IngredienteCadastroComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_ALTERAR_CATEGORIA'] }
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class IngredienteRoutingModule { }
