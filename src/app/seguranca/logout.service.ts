import { SisHttp } from './../seguranca/sis-http';
import { environment } from './../../environments/environment';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';

@Injectable()
export class LogoutService {

  tokenRevokeUrl: string;

  constructor(
    private http: SisHttp,
    private auth: AuthService
  ) {
    this.tokenRevokeUrl = `${environment.apiUrl}/tokens/revoke`;
  }

  logout() {
    return this.http.delete(this.tokenRevokeUrl,
      { withCredentials: true })
      .toPromise()
      .then(() => {
        this.auth.limparAcessToken();
      });
  }
}
