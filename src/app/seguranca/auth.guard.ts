/**
 * Classe do Guard para Controlar as Permissões
 * e Redirecionamento de acordo com a Validação do Token
 */
import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth.service';
import {Location} from '@angular/common';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private auth: AuthService,
    private router: Router,
    private location: Location
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    /**
     * Verifica se o Acess Token e o Refresh Token
     * estejam Inválidos/Expirados, com isso, ao tentar
     * Navegar em alguma funcionalidade ele será
     * automaticamente redirecionado para a Tela de Login.
     */
    if (this.auth.isAccessTokenInvalido()) {
      console.log('Navegação com Access Token Inválido. Obtendo Novo Token...');
      return this.auth.obterNovoAccessToken()
        .then(() => {
          if (this.auth.isAccessTokenInvalido()) {
            this.router.navigate(['/login']);
            return false;
          }
          return true;
        })
    // Verifica se o Usuário tem Acessos à Funcionalidade
    } else if (next.data.roles && !this.auth.verificarPermissoes(next.data.roles)) {
        // this.location.back();
        this.router.navigate(['/acesso-nao-autorizado']);
        return false;
    }

    return true;
  }

}
