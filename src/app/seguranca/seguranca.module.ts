/**
 * Serviço Auth de Http, onde será Fornecido para Toda a Autenticação,
 * pois está sendo Importando pelo app.module.js
 */
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';

import { SegurancaRoutingModule } from './seguranca-routing.module';
import { LoginFormComponent } from './login-form/login-form.component';
import { JwtModule } from '@auth0/angular-jwt';
import { AuthService } from './auth.service';
import { SisHttp } from './sis-http';
import { AuthGuard } from './auth.guard';
import { LogoutService } from './logout.service';
import {CardModule} from 'primeng/card';
import { environment } from '../../environments/environment';
import { HttpClientModule } from '@angular/common/http';

// Retorna o Token para a Confiuração do JwtModule do JWT Auth0
export function tokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    HttpClientModule,

    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter
      }
    }),
    InputTextModule,
    ButtonModule,
    CardModule,

    SegurancaRoutingModule
  ],
  declarations: [LoginFormComponent],
  providers: [
    AuthGuard,
    LogoutService
  ]
})
export class SegurancaModule { }
