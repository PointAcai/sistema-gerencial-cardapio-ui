import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Produto } from '../model/produto';
import { environment } from './../../environments/environment';
import { SisHttp } from './../seguranca/sis-http';

@Injectable()
export class ProdutoService {

  produtoUrl: string;

  constructor(private http: SisHttp) {
    this.produtoUrl = `${environment.apiUrl}/produtos`;
  }

  pesquisar(): Promise<any> {
    return this.http.get<any>(`${this.produtoUrl}?resumo`).toPromise();
  }

  // Método de Excluir
  excluir(codigo: number): Promise<void> {
    return this.http.delete<any>(`${this.produtoUrl}/${codigo}`).toPromise();
  }

  // Método de Adicionar
  adicionar(produto: Produto): Promise<Produto> {
    return this.http.post<Produto>(this.produtoUrl, produto).toPromise();
  }

  // Método de Atualizar
  atualizar(produto: Produto): Promise<Produto> {
    return this.http.put<Produto>(`${this.produtoUrl}/${produto.codigo}`, produto).toPromise();
  }

  // Método para Alterar a Propriedade Status
  alterarStatus(codigo: number, ativo: boolean): Promise<void> {
    const headers = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.put(`${this.produtoUrl}/${codigo}/status`, ativo, { headers} ).toPromise().then(() => null);
  }

  buscarPorCodigo(codigo: number): Promise<Produto> {
    return this.http.get<Produto>(`${this.produtoUrl}/${codigo}`).toPromise();
  }

  urlUploadImagem(): string {
    return `${this.produtoUrl}/imagem`
  }


  /** Listar Para Preencher os Combos */

  listarIngredientes(): Promise<any> {
    return this.http.get<any>(`${this.produtoUrl}/ingredientes`, { }).toPromise().then(response => {
        const ingredienteBanco = response;
        const resultado = {
          ingrediente: ingredienteBanco
        }
        return resultado;
      })
  }

}
