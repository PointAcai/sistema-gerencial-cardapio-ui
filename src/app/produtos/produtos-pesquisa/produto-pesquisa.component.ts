import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationService } from '../../../../node_modules/primeng/components/common/api';
import { ErrorHandlerService } from './../../core/error-handler.service';
import { Produto } from './../../model/produto';
import { AuthService } from './../../seguranca/auth.service';
import { ProdutoService } from './../produto.service';

@Component({
  selector: 'app-produto-pesquisa',
  templateUrl: './produto-pesquisa.component.html',
  styleUrls: ['./produto-pesquisa.component.css']
})
export class ProdutoPesquisaComponent implements OnInit {

  categorias: SelectItem[];

  produtos: Produto[];
  cols: any[];

  constructor(
    // Não Remover, pois a tela Utiliza para Validar os Botões
    private auth: AuthService,
    private produtoService: ProdutoService,
    private toastr: ToastrService,
    private confirmation: ConfirmationService,
    private errorHandler: ErrorHandlerService
  ) { }

  ngOnInit() {
    this.pesquisar();
  }

  pesquisar() {
    this.produtoService.pesquisar().then(resultado => {
      this.produtos = resultado;
    }).catch(erro => this.errorHandler.handle(erro));

    this.cols = [
      { field: 'numIdentificador', header: 'Código' },
      { field: 'descricao', header: 'Descrição' },
      { field: 'categoria', header: 'Categoria' },
      { field: 'valor', header: 'Valor' },
      { field: 'statusProduto', header: 'Status' },
      { field: 'exibirCardapio', header: 'Exibir no Cardápio?' },
      { field: 'codigo', header: '' }
    ];
  }

  confirmarExcluir(produto: any) {
    this.confirmation.confirm({
      message: 'Deseja Realmente Excluir este Produto?',
      accept: () => {
        this.excluir(produto);
      }
    });
  }

  excluir(produto: any) {
    this.produtoService.excluir(produto.codigo).then(() => {
      this.pesquisar();
      this.toastr.success('Produto Excluído com Sucesso!');
    }).catch(erro => this.errorHandler.handle(erro));
  }

  alterarStatus(produto: any): void {
    const novoStatus = !produto.statusProduto;

    this.produtoService.alterarStatus(produto.codigo, novoStatus).then(() => {
      const acao = novoStatus ? 'Ativado' : 'Desativado';
      produto.statusProduto = novoStatus;
      this.toastr.success(`Produto ${acao} com Sucesso!`);
    }).catch(erro => this.errorHandler.handle(erro));
  }

}
