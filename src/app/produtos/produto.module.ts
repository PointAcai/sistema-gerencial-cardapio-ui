import { ToolbarModule } from 'primeng/toolbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SelectButtonModule } from 'primeng/selectbutton';
import { TooltipModule } from 'primeng/tooltip';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { InputMaskModule } from 'primeng/inputmask';
import { MultiSelectModule } from 'primeng/multiselect';
import { DropdownModule } from 'primeng/dropdown';
import { SpinnerModule } from 'primeng/spinner';
import { RadioButtonModule } from 'primeng/radiobutton';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { CurrencyMaskModule } from 'ng2-currency-mask';

import { SharedModule } from './../shared/shared.module';
import { ProdutoRoutingModule } from './produto-routing.module';

import { ProdutoPesquisaComponent } from './produtos-pesquisa/produto-pesquisa.component';
import { ProdutoCadastroComponent } from './produtos-cadastro/produto-cadastro.component';
import { HttpClientModule } from '@angular/common/http';
import {FileUploadModule} from 'primeng/fileupload';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    InputTextModule,
    ButtonModule,
    TableModule,
    TooltipModule,
    SelectButtonModule,
    InputMaskModule,
    MultiSelectModule,
    DropdownModule,
    CurrencyMaskModule,
    SpinnerModule,
    RadioButtonModule,
    HttpClientModule,
    FileUploadModule,
    ProgressSpinnerModule,
    ToolbarModule,

    SharedModule,
    ProdutoRoutingModule
  ],
  declarations: [
    ProdutoCadastroComponent,
    ProdutoPesquisaComponent
  ],
  exports: []
})
export class ProdutoModule { }
