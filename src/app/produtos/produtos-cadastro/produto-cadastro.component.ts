import { IngredienteService } from './../../ingredientes/ingrediente.service';
import { SelectItem } from 'primeng/api';
import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';

import { ErrorHandlerService } from './../../core/error-handler.service';
import { ProdutoService } from './../produto.service';
import { Produto } from '../../model/produto';
import { Ingrediente } from '../../model/ingrediente';
import { UnidadeMedidaService } from 'app/unidadesmedida/unidademedida.service';
import { CategoriaService } from 'app/categorias/categoria.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-produto-cadastro',
  templateUrl: './produto-cadastro.component.html',
  styleUrls: ['./produto-cadastro.component.css'],
})
export class ProdutoCadastroComponent implements OnInit {

  formulario: FormGroup;

  unidadesMedida: SelectItem[];
  categorias: SelectItem[];
  ingredientesBase: Ingrediente[];
  ingredientes: Ingrediente[];

  uploadEmAndamento: Boolean = false;

  constructor(
    private produtoService: ProdutoService,
    private categoriaService: CategoriaService,
    private unidadeMedidaService: UnidadeMedidaService,
    private ingredienteService: IngredienteService,
    private errorHandler: ErrorHandlerService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {

    // Configuração do Formulário Reativo
    this.configurarFormularioReativo();

    this.listarIngredientes();

    this.listarCategorias();

    this.listarUnidadesMedida();

    const codigoProduto = this.route.snapshot.params['codigo'];
    if (codigoProduto) {
      this.carregarProduto(codigoProduto);
    }

  }

  // Momento em que o Upload será Realizado
  antesUploadImagem(event) {
    event.xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'));
    this.uploadEmAndamento = true;
  }

  aoTerminarUploadImagem(event) {
    const imagem = JSON.parse(event.xhr.response);
    this.formulario.patchValue({
      imagem: imagem.nome,
      urlImagem: imagem.url
    });
    this.uploadEmAndamento = false;
  }

  erroUpload(event) {
    this.toastr.error('Erro ao Tentar enviar Imagem do Produto.');
    this.uploadEmAndamento = false;
  }

  removerImagem() {
    this.formulario.patchValue({
      imagem: null,
      urlImagem: null
    });
  }

  get nomeAnexo() {
    const nome = this.formulario.get('imagem').value;
    if (nome) {
      return nome.substring(nome.indexOf('_') + 1, nome.length);
    }
    return '';
  }

  get urlUploadImagem() {
    return this.produtoService.urlUploadImagem();
  }

  configurarFormularioReativo() {
    this.formulario = this.formBuilder.group({
      codigo: null,
      numIdentificador: ['', [ this.validarObrigatoriedade ]],
      descricao: ['', [ this.validarObrigatoriedade ]],
      ingredientes: null,
      exibirCardapio: ['Sim', [ this.validarObrigatoriedade ]],
      categoria: this.formBuilder.group({
        codigo: [null, [ this.validarObrigatoriedade ]]
      }),
      unidadeMedida: this.formBuilder.group({
        codigo: null
      }),
      quantidadeMedida: null,
      valor: ['', [ this.validarObrigatoriedade ]],
      imagem: [],
      urlImagem: [],
      tipoComplemento: []
    });
  }

  /**
   * VALIDAÇÕES
   */

  validarObrigatoriedade(input: FormControl) {
    return (input.value ? null : { obrigatoriedade: true });
  }

  listarCategorias() {
    this.categoriaService.listarCategorias()
      .then(categoria => {
        const categoriasBanco = categoria.content;
        this.categorias = categoriasBanco.map(p =>
          ({ label: p.descricao, value: p.codigo })
        );
        this.categorias.splice(0, 0, {label: 'Selecione uma Categoria', value: null});
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  listarUnidadesMedida() {
    this.unidadeMedidaService.listarUnidadesMedida()
      .then(unidadesMedida => {
        const unidadesMedidaBanco = unidadesMedida.content;
        this.unidadesMedida = unidadesMedidaBanco.map(p =>
          ({ label: p.descricao, value: p.codigo })
        );
        this.unidadesMedida.splice(0, 0, {label: 'Selecione uma Unidade de Medida', value: null});
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  listarIngredientes() {
    this.produtoService.listarIngredientes()
      .then(ingredientes => {
        const ingredienteBanco = ingredientes.ingrediente;
        this.ingredientes = ingredienteBanco;
        this.ingredientesBase = ingredienteBanco;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  carregarProduto(codigo: number) {
    this.produtoService.buscarPorCodigo(codigo)
      .then(produto => {
        this.formulario.patchValue(produto);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  get editando() {
    return Boolean(this.formulario.get('codigo').value);
  }

  salvar() {
    if (this.editando) {
      this.atualizarProduto();
    } else {
      this.adicionarProduto();
    }
  }

  adicionarProduto() {
    this.produtoService.adicionar(this.formulario.value)
      .then(produtoAdicionado => {
        this.toastr.success('Produto Adicionado com Sucesso!');
        this.router.navigate(['/produtos']);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  atualizarProduto() {
    this.produtoService.atualizar(this.formulario.value)
      .then(produto => {
        this.formulario.patchValue(produto);
        this.toastr.success('Produto Atualizado com Sucesso!');
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  novo() {
    this.formulario.reset();
    setTimeout(function() {
      this.produto = new Produto();
    }.bind(this), 1);
    this.router.navigate(['/produtos/cadastrar']);
  }

}
