import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { environment } from './../../environments/environment';
import { Usuario } from './../model/usuario';
import { SisHttp } from './../seguranca/sis-http';

@Injectable()
export class UsuarioService {

  usuarioUrl: string;

  constructor(private http: SisHttp) {
    this.usuarioUrl = `${environment.apiUrl}/usuarios`;
  }

  pesquisar(): Promise<any> {
    return this.http.get<any>(`${this.usuarioUrl}?resumo`).toPromise();
  }

  excluir(codigo: number): Promise<void> {
    return this.http.delete<any>(`${this.usuarioUrl}/${codigo}`).toPromise();
  }

  alterarStatus(codigo: number, ativo: boolean): Promise<void> {
    const headers = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.put(`${this.usuarioUrl}/${codigo}/status`, ativo, { headers} ).toPromise().then(() => null);
  }

  adicionar(usuario: Usuario): Promise<Usuario> {
    return this.http.post<Usuario>(this.usuarioUrl,  usuario).toPromise();
  }

  atualizar(usuario: Usuario): Promise<Usuario> {
    return this.http.put<Usuario>(`${this.usuarioUrl}/${usuario.codigo}`, usuario).toPromise();
  }

  buscarPorCodigo(codigo: number): Promise<Usuario> {
    return this.http.get<Usuario>(`${this.usuarioUrl}/${codigo}`).toPromise();
  }

}
