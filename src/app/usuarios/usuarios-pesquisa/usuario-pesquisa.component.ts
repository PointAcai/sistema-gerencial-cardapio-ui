import { Component, OnInit } from '@angular/core';
import { Usuario } from 'app/model/usuario';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationService } from '../../../../node_modules/primeng/components/common/api';
import { ErrorHandlerService } from './../../core/error-handler.service';
import { AuthService } from './../../seguranca/auth.service';
import { UsuarioService } from './../usuario.service';

@Component({
  selector: 'app-usuario-pesquisa',
  templateUrl: './usuario-pesquisa.component.html',
  styleUrls: ['./usuario-pesquisa.component.css']
})
export class UsuarioPesquisaComponent implements OnInit {

  usuarios: Usuario[];
  cols: any[];

  constructor(
    private auth: AuthService,
    private usuarioService: UsuarioService,
    private toastr: ToastrService,
    private confirmation: ConfirmationService,
    private errorHandler: ErrorHandlerService
  ) { }

  ngOnInit() {
    this.pesquisar();
  }

  pesquisar() {
    this.usuarioService.pesquisar().then(resultado => {
      this.usuarios = resultado
    }).catch(erro => this.errorHandler.handle(erro));

    this.cols = [
      { field: 'codigo', header: 'Código' },
      { field: 'nome', header: 'Nome' },
      { field: 'login', header: 'Login' },
      { field: 'perfil', header: 'Perfil' },
      { field: 'status', header: 'Status' },
      { field: 'codigo', header: '' }
    ];
  }

  confirmarExcluir(usuario: any) {
    this.confirmation.confirm({
      message: 'Deseja Realmente Excluir este Usuário?',
      accept: () => {
        this.excluir(usuario);
      }
    });
  }

  excluir(usuario: any) {
    this.usuarioService.excluir(usuario.codigo).then(() => {
      this.pesquisar();
      this.toastr.success('Usuário Excluído com Sucesso!');
    })
    .catch(erro => this.errorHandler.handle(erro));
  }

  alterarStatus(usuario: any): void {
    const novoStatus = !usuario.status;
    this.usuarioService.alterarStatus(usuario.codigo, novoStatus).then(() => {
      const acao = novoStatus ? 'Ativado' : 'Desativado';
      usuario.status = novoStatus;
      this.toastr.success(`Usuário ${acao} com Sucesso!`);
    }).catch(erro => this.errorHandler.handle(erro));
  }

}
