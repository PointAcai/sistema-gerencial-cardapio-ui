import { Component, OnInit } from '@angular/core';

import { FormControl, FormGroup, FormBuilder } from '../../../../node_modules/@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '../../../../node_modules/@angular/router';

import { ErrorHandlerService } from './../../core/error-handler.service';
import { PerfilService } from './../../perfis/perfil.service';
import { Usuario } from './../../model/usuario';
import { UsuarioService } from './../usuario.service';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-usuario-cadastro',
  templateUrl: './usuario-cadastro.component.html',
  styleUrls: ['./usuario-cadastro.component.css']
})
export class UsuarioCadastroComponent implements OnInit {

  formulario: FormGroup;

  perfis: SelectItem[];

  constructor(
    // Responsável pelos Serviços que o Perfil fornece
    private perfilService: PerfilService,
    // Apresenta os Erros
    private errorHandler: ErrorHandlerService,
    // Responsável pelos Serviços que o Usuário fornece
    private usuarioService: UsuarioService,
    // Apresenta mensagens na Tela
    private toastr: ToastrService,
    // Através dele é possivel verificar qual a Rota Sendo Chamada
    private route: ActivatedRoute,
    // Possibilita a Navega Imperativa, Levando para outras Páginas
    private router: Router,
    // Responsável pela Configuração do Formulário Reativo
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    // Configuração do Formulário Reativo
    this.configurarFormularioReativo();
    const codigoUsuario = this.route.snapshot.params['codigo'];

    if (codigoUsuario) {
      this.carregarUsuario(codigoUsuario);
    }
    this.listarPerfis();
  }

  configurarFormularioReativo() {
    this.formulario = this.formBuilder.group({
      codigo: '',
      nome: [ '', [ this.validarObrigatoriedade ] ],
      login: [ '', [ this.validarObrigatoriedade ] ],
      senha: [ '', [ this.validarObrigatoriedade ] ],
      confirmarSenha: [ '', [ this.validarObrigatoriedade ] ],
      perfil: this.formBuilder.group({
        codigo: [ '', [ this.validarObrigatoriedade ] ]
      }),
    });
  }

  carregarUsuario(codigo: number) {
    this.usuarioService.buscarPorCodigo(codigo)
      .then(usuario => {
        this.formulario.patchValue(usuario);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  get editando() {
    return Boolean(this.formulario.get('codigo').value);
  }

  listarPerfis() {
    this.perfilService.listarPerfis()
      .then(perfis => {
        const perfisBanco = perfis.content;
        this.perfis = perfisBanco.map(p =>
          ({ label: p.descricao, value: p.codigo })
        );
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  salvar() {
    if (this.editando) {
      this.atualizarUsuario();
    } else {
      this.adicionarUsuario();
    }
  }

  adicionarUsuario() {
    this.usuarioService.adicionar(this.formulario.value)
      .then(usuarioAdicionado => {
        this.toastr.success('Usuário Adicionado com Sucesso!');
        this.router.navigate(['/usuarios']);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  atualizarUsuario() {
    this.usuarioService.atualizar(this.formulario.value)
      .then(usuario => {
        this.formulario.patchValue(usuario);
        this.toastr.success('Usuário Atualizado com Sucesso!');
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  novo() {
    this.formulario.reset();
    // POG para permitir o Tempo de Carregamento do Form
    setTimeout(function() {
      this.usuario = new Usuario();
    }.bind(this), 1);
    this.router.navigate(['/usuarios/novo']);
  }

  /**
   * VALIDAÇÕES
   */

  validarObrigatoriedade(input: FormControl) {
    if (!input.root.get('codigo') || input.root.get('codigo').value === '') {
      return (input.value ? null : { obrigatoriedade: true });
    } else {
      if (input.root.get('senha') || input.root.get('confirmarSenha')) {
        if (input.root.get('senha').value) {
          return (input.value ? null : { obrigatoriedade: true });
        }
        return null;
      }
    }
  }

  validarCombinacaoSenha(input: FormControl) {
    if (!input.root.get('codigo') || input.root.get('codigo').value === '') {
      const senha = input.root.get('senha');
      const confirmarSenha = input.root.get('confirmarSenha');

      if (senha && confirmarSenha) {
        return (senha.value === confirmarSenha.value ? null : { senhasNaoCoincidem: true });
      }
      return null;
    } else {
      return null;
    }
  }

}
