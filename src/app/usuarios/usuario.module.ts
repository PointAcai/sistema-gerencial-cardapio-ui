import { MultiSelectModule } from 'primeng/multiselect';
import { UsuarioRoutingModule } from './usuario-routing.module';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';
import { DropdownModule } from 'primeng/dropdown';
import { SelectButtonModule } from 'primeng/selectbutton';
import { CalendarModule } from 'primeng/calendar';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { TooltipModule } from 'primeng/tooltip';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { InputMaskModule } from 'primeng/inputmask';
import { PasswordModule } from 'primeng/password';
import { ToolbarModule } from 'primeng/toolbar';

import { SharedModule } from './../shared/shared.module';
import { UsuarioPesquisaComponent } from './usuarios-pesquisa/usuario-pesquisa.component';
import { UsuarioCadastroComponent } from './usuarios-cadastro/usuario-cadastro.component';

import { DataTableModule } from 'primeng/datatable';
import { from } from 'rxjs';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    InputTextModule,
    ButtonModule,
    TableModule,
    TooltipModule,
    InputTextareaModule,
    CalendarModule,
    SelectButtonModule,
    DropdownModule,
    CurrencyMaskModule,
    InputMaskModule,
    PasswordModule,
    DataTableModule,
    MultiSelectModule,
    ToolbarModule,

    SharedModule,
    UsuarioRoutingModule
  ],
  declarations: [
    UsuarioCadastroComponent,
    UsuarioPesquisaComponent
  ],
  exports: []
})
export class UsuarioModule { }
