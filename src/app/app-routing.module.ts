import { AcessoNaoAutorizadoComponent } from './core/acesso-nao-autorizado.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { PaginaNaoEncontradaComponent } from './core/pagina-nao-encontrada.component';

// Módulo de Roteamento
/*
  Configurando as Rotas
  Os Objetos Representam a Configuração de Cada Roteamento

  Obs: Não Esquecer de Importar o Route Module
*/
const routes: Routes = [
  // Rotas Funcionalidades e Configuração de Carregamento Tardio (Lazy Loading)
  { path: 'dashboard', loadChildren: 'app/dashboard/dashboard.module#DashboardModule' },
  { path: 'usuarios', loadChildren: 'app/usuarios/usuario.module#UsuarioModule' },
  { path: 'perfis', loadChildren: 'app/perfis/perfil.module#PerfilModule' },
  { path: 'produtos', loadChildren: 'app/produtos/produto.module#ProdutoModule' },
  { path: 'ingredientes', loadChildren: 'app/ingredientes/ingrediente.module#IngredienteModule' },
  { path: 'unidadesmedida', loadChildren: 'app/unidadesmedida/unidademedida.module#UnidadeMedidaModule' },
  { path: 'categorias', loadChildren: 'app/categorias/categoria.module#CategoriaModule' },
  { path: 'ofertas', loadChildren: 'app/ofertas/oferta.module#OfertaModule' },
  { path: 'cardapio', loadChildren: 'app/cardapio/cardapio.module#CardapioModule' },
  { path: 'parametros', loadChildren: 'app/parametros/parametro.module#ParametroModule' },
  { path: 'controlepedidos', loadChildren: 'app/controlepedidos/controlepedido.module#ControlePedidoModule' },
 
  // Rotas Genéricas
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'pagina-nao-encontrada', component: PaginaNaoEncontradaComponent },
  { path: 'acesso-nao-autorizado', component: AcessoNaoAutorizadoComponent },
  { path: '**', redirectTo: 'pagina-nao-encontrada' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
