import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-acesso-nao-autorizado',
  template: `
    <div class="container">
      <h1 class="text-center">Acesso não Autorizado! =(</h1>
    </div>
  `,
  styles: []
})
export class AcessoNaoAutorizadoComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
