// Módulo que Deixa os Services Disponivel Para Toda Aplicação
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';

import { ToastrModule } from 'ngx-toastr';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';

import { NavbarComponent } from './navbar/navbar.component';
import { ErrorHandlerService } from './error-handler.service';
import { UsuarioService } from './../usuarios/usuario.service';
import { AuthService } from './../seguranca/auth.service';
import { PerfilService } from './../perfis/perfil.service';
import { PermissaoService } from './../perfis/permissao.service';
import { ProdutoService } from '../produtos/produto.service';
import { CardapioService } from './../cardapio/cardapio.service';
import { UnidadeMedidaService } from './../unidadesmedida/unidademedida.service';
import { IngredienteService } from './../ingredientes/ingrediente.service';
import { OfertaService } from 'app/ofertas/oferta.service';
import { CategoriaService } from 'app/categorias/categoria.service';
import { SisHttp } from './../seguranca/sis-http';

import { PaginaNaoEncontradaComponent } from './pagina-nao-encontrada.component';
import { Title } from '../../../node_modules/@angular/platform-browser';
import { AcessoNaoAutorizadoComponent } from './acesso-nao-autorizado.component';
import { ParametrosService } from 'app/parametros/parametro.service';
import { ControlePedidoService } from 'app/controlepedidos/controlepedido.service';

// Define o LocalePt para toda a Aplicação!
registerLocaleData(localePt);

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    // Módulo de Configuração das Mensagens de Validação
    ToastrModule.forRoot(),
    // Módulo de Component do Dialog
    ConfirmDialogModule,
    // Módulo de Configuração das Rotas
    RouterModule
  ],
  declarations: [NavbarComponent, PaginaNaoEncontradaComponent, AcessoNaoAutorizadoComponent],
  exports: [
    NavbarComponent,
    ToastrModule,
    ConfirmDialogModule
  ],
  providers: [
    // Declaração dos Services que serão usados na Aplicação
    UsuarioService,
    PerfilService,
    PermissaoService,
    ErrorHandlerService,
    AuthService,
    ProdutoService,
    CategoriaService,
    UnidadeMedidaService,
    IngredienteService,
    OfertaService,
    CardapioService,
    ParametrosService,
    ControlePedidoService,
    SisHttp,

    ConfirmationService,
    JwtHelperService,
    Title,
    {provide: LOCALE_ID, useValue: 'pt'}
  ]
})
export class CoreModule { }
