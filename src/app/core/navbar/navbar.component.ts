import { Component, OnInit, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { ErrorHandlerService } from './../error-handler.service';
import { LogoutService } from './../../seguranca/logout.service';
import { AuthService } from './../../seguranca/auth.service';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  exibindoMenu = false;

  constructor(
    private logoutService: LogoutService,
    private errorHandle: ErrorHandlerService,
    private router: Router,
    private elementRef: ElementRef,
    /**
     * Anotação auth para Verificar se o Usuário tem Acesso
     * à determinada Funcionalidade presente no MENU de Acesso
     */
    public auth: AuthService
  ) { }

  ngOnInit() {

  }

  @HostListener('document:click', ['$event'])
  public aoClicar(event) {
    const elementoClicado = event.target;
    const estaDentro = this.elementoClicadoEstaDentroDoMenu(elementoClicado, this.elementRef);
    if (!estaDentro) {
      this.exibindoMenu = false;
    }
  }

  private elementoClicadoEstaDentroDoMenu(elementoClicado: any, elementRef: ElementRef) {
    while (elementoClicado) {
      if (elementoClicado === elementRef.nativeElement) {
        return true;
      }
      elementoClicado = elementoClicado.parentNode;
    }
    return false;
  }

  logout() {
    this.logoutService.logout()
      .then(() => {
        this.router.navigate(['/login']);
      })
      .catch(erro => this.errorHandle.handle(erro));
  }

}
