import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { ToastContainerDirective, ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  @ViewChild(ToastContainerDirective, {static: true} as any) toastContainer: ToastContainerDirective;

  // Configuração do Toastr, Biblioteca de Mensagens Validação
  constructor(
    private toastrService: ToastrService,
    private router: Router
  ) {  }

  ngOnInit() {
    this.toastrService.overlayContainer = this.toastContainer;
  }

  exibindoNavbar() {
    return ((this.router.url !== '/login') && (this.router.url !== '/cardapio'));
  }

}
